<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distribution extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
  public function __construct(){
  parent::__construct();
	  $this->load->helper('date');
	   $this->table="distribution";
 }

	public function index()
	{
		if($this->fb_rest->isloggedin()){
		$data = array();
		$page_no = $this->uri->segment('2');
		$per_page = $this->input->get_post("no_items", true);
		$search = $this->input->get_post("search", true);
		$sort_fld = $this->input->get_post("sort_fld", true);
		$sort_dir = $this->input->get_post("sort_dir", true);
		$page_burl = site_url("/distribution");
		$table_name = $this->table;
		$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
		"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
		//$params = array();
		  // or  $params = compact("page_no", "per_page", "search", "sort_fld", "sort_dir", "page_burl", "table_name");
		  
		  $data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns =array("from_pond", "to_pond", "species_type", "count", "updatedby", "distribution_date");
			
			$hstr = array("from_pond" => fb_text("from_pond"), "to_pond" => fb_text("to_pond"), "species_type" => fb_text("species_type"), "count" => fb_text("count"), "updatedby" => fb_text("updated_by"), "distribution_date" => fb_text("date"), "action" => fb_text("action"));
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/distribution?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;  
			
		$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/distribution_content", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
		
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
		
	}
	
	function create(){
		$table_name=$this->table;
		$form_data = $this->input->post();
		$form_data['updatedtime'] = now();	
		$form_data['createdtime'] = now();
		$form_date = $this->input->post("date");
		$form_data['distribution_date'] = fb_convert_time($form_date);
		$date = explode('/', $form_date);
		$form_data['_month'] = $date[0];
		$form_data['_day']   = $date[1];
		$form_data['_year']  = $date[2];
		
		// From Pond id and name update
		$fpid = $form_data["from_pond"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $fpid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["from_pond"] = $pname;
		$form_data["from_pond_id"] = $fpid;
		
		// To Pond id and name update
		$tpid = $form_data["to_pond"]; // Actually get pond id		
		$crst = $this->fb_rest->get_record("ponds", $tpid);
		$rst = $crst["result_set"];
		$pname = $rst["pondname"];
		$form_data["to_pond"] = $pname;
		$form_data["to_pond_id"] = $tpid;

		
		// Species type and Species ID update
		$sid = $form_data["species_type"]; // Actually get Species id
		$crst1 = $this->fb_rest->get_record("species", $sid);
		$rst1 = $crst1["result_set"];
		$stype = $rst1["species_type"];
		$form_data["species_type"] = $stype;
		$form_data["species_id"] = $sid;

		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success','Record Created');
			$this->fb_rest->update_current_stock($form_data["from_pond"], $form_data["species_type"], $form_data["count"], 'reduce', $fpid, $sid);
			$this->fb_rest->update_current_stock($form_data["to_pond"], $form_data["species_type"], $form_data["count"], 'add', $tpid, $sid);
			redirect('/distribution');
		}else{
			$this->session->set_flashdata('failed','Insert failed, Try Again!');
			redirect('/distribution');
		}
	}
	
	function delete(){
		$table_name=$this->table;
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success','Record Deleted');
			redirect('/distribution');
		}else{
			$this->session->set_flashdata('delete_failed','Failed, Please Try Again!');
			redirect('/distribution');
		}
	}
	
   public function getcurrentStock(){
	   $pond_id = $this->input->post("pond_id");
		$table_name = "current_stock";
		$from = 0;
		$size = 1;
		$orderfld = "updatedtime";
		$orderdir = "desc";
		
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir, "pond_id" => $pond_id);
		$query_str = $this->parser->parse('query/currentstock', $qpms, true);
	//	fb_pr($query_str);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
//		fb_pr($result);
	   echo json_encode($result,true);
   }


	
}
