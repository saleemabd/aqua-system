<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$this->benchmark->mark('current_stock_api_start');
		$cstockTable = "current_stock";
		$stock = $this->fb_rest->search_list($cstockTable,'');	
		$data['stocks'] = isset($stock["result_set"])?$stock["result_set"]:array();
		$this->benchmark->mark('current_stock_api_end');
		$this->benchmark->mark('week_reports_api_start');
		$data['wk_reports'] = $this->fb_rest->last_weeks_reports();
		$this->benchmark->mark('week_reports_api_end');
		$this->benchmark->mark('activities_api_start');
		$data['activities'] = $this->getActivity();
		$this->benchmark->mark('activities_api_end');
		
		$this->benchmark->mark('sampling_api_start');
		$data["asampling"] = $this->fb_rest->get_sampling();
		$this->benchmark->mark('sampling_api_end');
		
		if($this->fb_rest->isloggedin()){
			$this->benchmark->mark('header_layout_start');
  		    $this->load->view('include/header');
			$this->benchmark->mark('header_layout_end');
			
			$this->benchmark->mark('left_menu_start');
			$this->load->view('include/left_menu');
			$this->benchmark->mark('left_menu_end');
			
			$this->benchmark->mark('dashboard_content_render_start');
			$this->load->view('layout/dashboard_content',$data);
			$this->benchmark->mark('dashboard_content_render_end');
			
			$this->benchmark->mark('footer_layout_start');
			$this->load->view('include/footer');
			$this->benchmark->mark('footer_layout_end');
			
		}else{
			redirect('/login');
		}
	}
	
	public function getCurrentStock(){
		$table_name = "current_stock";
		$stocks = $this->fb_rest->search_list($table_name,'');		
		echo json_encode($stocks);
	}
	
	private function last_weeks_reports(){
		$file_name = "last_week_reports";
		$this->load->driver('cache', array('adapter' => 'file'));
		if (! $lweek_rep_str = $this->cache->get($file_name))
		{
			$ctime = now();
			$lastweek1 = strtotime("-1 week midnight");
			$lastweek2 = strtotime("-2 week midnight");
			$total_rst = array();
			
			$arep_params = array(
			  "harvest" => array(
				"this_week" =>	array("size" => 100, "from" => 0, 
						"range_fld" => "harvest_date",
						"orderfld" => "updatedtime", "orderdir" => "desc",
						"topv" => $ctime,
						"lowv" => $lastweek1),
				"last_week" =>	array("size" => 100, "from" => 0, 
						"range_fld" => "harvest_date",
						"orderfld" => "updatedtime", "orderdir" => "desc",
						"topv" => $lastweek1-1,
						"lowv" => $lastweek2)
			   ),
			  "mortality" => array( "this_week" =>
					array("size" => 100, "from" => 0, 
						"range_fld" => "mortality_date",
						"orderfld" => "updatedtime", "orderdir" => "desc",
						"topv" => $ctime,
						"lowv" => $lastweek1),
				"last_week" =>	array("size" => 100, "from" => 0, 
						"range_fld" => "mortality_date",
						"orderfld" => "updatedtime", "orderdir" => "desc",
						"topv" => $lastweek1-1,
						"lowv" => $lastweek2)
			   ),
			   
			   
			);
			
			foreach($arep_params as $ctbl => $aparams){
				foreach($aparams as $wk => $qpms){
					$query_str = $this->parser->parse('query/query-range', $qpms, true);
					$result = $this->fb_rest->get_query_result($ctbl, $query_str);
					
					$stat = $result["status"];
					$cur_rst = !empty($result["result_set"]) ? $result["result_set"] : array();
					if($stat=="success" && !empty($cur_rst)){
						$total_rst[$ctbl][$wk] = $cur_rst;
					}
				}
			}
			
			if(!empty($total_rst)){
				$lweek_rep_str = json_encode($total_rst);
				if(chk_rst_cache()){
					$this->cache->save($file_name, $lweek_rep_str, 86400);
				}
			}
			return $total_rst;
		} else {
			$total_rst = json_decode($lweek_rep_str, true);
			return $total_rst;
		}
		
	}
	
	public function getActivity(){
	 $ci =& get_instance();
	  $table_name = "activities";
	  $result = $ci->fb_rest->search_list($table_name,'',10);	
	  //print_r($result);
	  $activity = isset($result["result_set"])?$result["result_set"]:array();
	  return $activity;

}
	
}
