<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller {

	public function __construct()
	{
        parent::__construct();
		if(!$this->fb_rest->isloggedin())
		{
			redirect("/login");
		}
	}
	
	public function index()
	{
		echo "this is common controller";
	}
	
	public function check_duplicate(){
		$search = $this->input->get_post("search", true);
		$fld_name = $this->input->get_post("fld_name", true);
		$table_name = $this->input->get_post("table_name", true);
		$rkey = $this->input->get_post("rkey", true);
		
		$rst = $this->fb_rest->check_duplicate($table_name,$fld_name,$search,$rkey);
		echo json_encode($rst);
		exit;
	}
	
	public function change_lang(){
		$lang = $this->input->get_post("lang");
		$ruri = $this->input->get_post("ruri");
		$ruri = !empty($ruri)? "/".$ruri : "/dashboard";
		$clang = $this->config->item('language', 'fb_boodskap');
		$alang = array_keys($clang);
		$lang = (in_array($lang, $alang)) ? $lang : "en";
		$this->session->set_userdata("lang", $lang);
		fb_common_js();
		redirect($ruri);
	}
	
	function getActivities(){
		$page_no = $this->input->get("page");
		$per_page=10;	 
		$from = ($page_no <= 1) ? "0" : ( $page_no - 1 ) ;
		$from = ($from * $per_page);

		$table_name = "activities";
		$result = $this->fb_rest->lod_more($table_name,$from,$per_page);	

		$activities = $result['result_set'];
		if(!empty($activities)) {
			foreach($activities as $activity): 
				$_source = $activity['_source'];
				$activityArr= array();
				$activityArr= unserialize($_source['activity_ref']);
				//print_r($activityArr);
		?>
			<div class="feed-item">
			  <div class="date">
				<?=  date("d F Y H:i:s", $_source['createdtime']) ?>
			  </div>
			  <div class="text">
				<?= $activityArr['msg']; ?>
			  </div>
			</div>
			<?php endforeach; 
		}

		//$activity = $result['result_set'];		
		//return $activity;

	}
	
	public function common_js(){
		$this->load->library('parser');
		$params["site_url"] = site_url("/");
		$ajs_lang = fb_jslang();
		$params["site_err_lang"] = json_encode($ajs_lang);
		$cjs = $this->parser->parse('include/common_js', $params, true);
		$this->output
			//->set_content_type('application/javascript')
			->set_output($cjs);
	}
	
	function get_Currentstock(){
		$pid = $this->input->post("pond");
		$sid = $this->input->post("species_type");
		$table_name="current_stock";
		
		
		$record= $this->fb_rest->get_currentStock($table_name, $pid);

		if($record["status"] == "success")
		 {	
		 	$result= $record["result_set"];
			//print_r($result);
			$source = $result['_source'];
			$pond_id = $source["pond_id"];
			if($pond_id==$pid)
		 	echo json_encode($result);
			else
			echo json_encode(array("status" => "failed"));
		 	
		 }else{
			
			
			echo json_encode(array("status" => "failed"));
		 }
	}


	public function settings()
	{
		$data = array();
		$this->config->load("fb_settings", TRUE);
		$asettings = $this->config->item("settings", "fb_settings");
		$data['asettings'] = $asettings;
		$this->load->view('include/header');
		$this->load->view('include/left_menu');
		$this->load->view('layout/settings_content', $data);
		$this->load->view('include/footer');
	}
	
	public function save_settings()
	{
		$post = $this->input->post();
		$udata = array();
		$this->config->load("fb_settings", TRUE);
		$asettings = $this->config->item("settings", "fb_settings");
		$frst_set = $asettings["rest_api_cache"];
		
		
		$cstr = "";
		foreach($asettings as $skey => $sval){
			$cur_pval = $this->input->post($skey, true);
			$cur_val = ($cur_pval)? "true" : "false";
			//$udata[$skey] = $cur_val;
			$pstr = sprintf('$config["settings"]["%s"] = %s;'."\n",$skey, $cur_val);
			$cstr .= $pstr;
		}
		$ctxt = <<<EOT
<?php
/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */

$cstr

/* End of file */
EOT;


		$config_footer = "/* End of file */";
		//highlight_string ($ctxt);
		$file = "fb_settings.php";
		file_put_contents(APPPATH."config/$file",utf8_encode($ctxt),LOCK_EX);
		
		$nrst_set = $this->input->post("rest_api_cache");
		
		// $str = "Old value : $frst_set , new value : $nrst_set";
		
		if($nrst_set!=$frst_set){
			$this->load->driver('cache', array('adapter' => 'file'));
			$this->cache->clean();
		}
		
		$this->session->set_flashdata('config_success', "Successfully updated settings.");
		redirect("/common/settings");
	}
	
	
	public function purge_caches(){
		// $this->load->driver('cache', array('adapter' => 'file'));
		// $this->cache->clean();
		// "All caches were purged."

		$data = array();
		$this->load->view('include/header');
		$this->load->view('include/left_menu');
		$this->load->view('layout/pcache_content', $data);
		$this->load->view('include/footer');
	}
	
	public function purge_confirm_caches(){
		$this->load->driver('cache', array('adapter' => 'file'));
		$this->cache->clean();
		$this->fb_rest->prepare_cache();
		$this->session->set_flashdata('config_success', "All caches were purged.");
		redirect("/common/purge_caches");
	}

}
