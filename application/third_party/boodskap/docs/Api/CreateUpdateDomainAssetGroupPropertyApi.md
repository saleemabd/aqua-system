# Swagger\Client\CreateUpdateDomainAssetGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**storeDomainAssetGroupProperty**](CreateUpdateDomainAssetGroupPropertyApi.md#storeDomainAssetGroupProperty) | **POST** /domain/asset/group/property/upsert/{atoken} | Create / Update Domain Asset Group Property


# **storeDomainAssetGroupProperty**
> \Swagger\Client\Model\Success storeDomainAssetGroupProperty($atoken, $entity)

Create / Update Domain Asset Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateDomainAssetGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$entity = new \Swagger\Client\Model\DomainAssetGroupProperty(); // \Swagger\Client\Model\DomainAssetGroupProperty | DomainAssetGroupProperty JSON object

try {
    $result = $apiInstance->storeDomainAssetGroupProperty($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateDomainAssetGroupPropertyApi->storeDomainAssetGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **entity** | [**\Swagger\Client\Model\DomainAssetGroupProperty**](../Model/DomainAssetGroupProperty.md)| DomainAssetGroupProperty JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

