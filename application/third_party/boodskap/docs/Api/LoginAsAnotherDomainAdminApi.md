# Swagger\Client\LoginAsAnotherDomainAdminApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginAs**](LoginAsAnotherDomainAdminApi.md#loginAs) | **GET** /domain/loginas/{email}/{passwd}/{dkey} | Login as another domain admin


# **loginAs**
> \Swagger\Client\Model\UserDomain loginAs($email, $passwd, $dkey)

Login as another domain admin

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LoginAsAnotherDomainAdminApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$email = "email_example"; // string | System admin user name/email
$passwd = "passwd_example"; // string | 
$dkey = "dkey_example"; // string | Domain key

try {
    $result = $apiInstance->loginAs($email, $passwd, $dkey);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginAsAnotherDomainAdminApi->loginAs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**| System admin user name/email |
 **passwd** | **string**|  |
 **dkey** | **string**| Domain key |

### Return type

[**\Swagger\Client\Model\UserDomain**](../Model/UserDomain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

