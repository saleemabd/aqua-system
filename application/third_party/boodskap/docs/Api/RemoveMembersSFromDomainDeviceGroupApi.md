# Swagger\Client\RemoveMembersSFromDomainDeviceGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**removeMembersFromDomainDeviceGroup**](RemoveMembersSFromDomainDeviceGroupApi.md#removeMembersFromDomainDeviceGroup) | **POST** /domain/device/group/remove/{atoken}/{gid} | Remove Members(s) from Domain Device Group


# **removeMembersFromDomainDeviceGroup**
> \Swagger\Client\Model\Success removeMembersFromDomainDeviceGroup($atoken, $gid, $device_ids)

Remove Members(s) from Domain Device Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemoveMembersSFromDomainDeviceGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group Id
$device_ids = array(new \Swagger\Client\Model\string[]()); // string[] | Array of device IDs

try {
    $result = $apiInstance->removeMembersFromDomainDeviceGroup($atoken, $gid, $device_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoveMembersSFromDomainDeviceGroupApi->removeMembersFromDomainDeviceGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group Id |
 **device_ids** | **string[]**| Array of device IDs |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

