# Swagger\Client\RetreiveDomainAssetGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDomainAssetGroupProperty**](RetreiveDomainAssetGroupPropertyApi.md#getDomainAssetGroupProperty) | **GET** /domain/asset/group/property/get/{atoken}/{gid}/{name} | Retreive Domain Asset Group Property


# **getDomainAssetGroupProperty**
> \Swagger\Client\Model\DomainAssetGroupProperty getDomainAssetGroupProperty($atoken, $gid, $name)

Retreive Domain Asset Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveDomainAssetGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | Group ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getDomainAssetGroupProperty($atoken, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDomainAssetGroupPropertyApi->getDomainAssetGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**| Group ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\DomainAssetGroupProperty**](../Model/DomainAssetGroupProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

