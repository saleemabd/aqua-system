# Swagger\Client\SearchApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search**](SearchApi.md#search) | **POST** /search/query/{atoken}/{type}/ | Search


# **search**
> \Swagger\Client\Model\SearchResult search($atoken, $type, $query, $id, $repositary, $mapping)

Search

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SearchApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | 
$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
$id = 56; // int | Required if **type** is one of **[ message | command | record ]**
$repositary = "repositary_example"; // string | Required if **type** is one of **GLOBAL**
$mapping = "mapping_example"; // string | Required if **type** is one of **GLOBAL**

try {
    $result = $apiInstance->search($atoken, $type, $query, $id, $repositary, $mapping);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchApi->search: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**|  |
 **query** | [**\Swagger\Client\Model\SearchQuery**](../Model/SearchQuery.md)| SearchQuery JSON |
 **id** | **int**| Required if **type** is one of **[ message | command | record ]** | [optional]
 **repositary** | **string**| Required if **type** is one of **GLOBAL** | [optional]
 **mapping** | **string**| Required if **type** is one of **GLOBAL** | [optional]

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

