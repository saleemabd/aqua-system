# Swagger\Client\CountAllMessagesApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countAllMessages**](CountAllMessagesApi.md#countAllMessages) | **POST** /message/count/{atoken}/{id} | Count All Messages


# **countAllMessages**
> \Swagger\Client\Model\Count countAllMessages($atoken, $id)

Count All Messages

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CountAllMessagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$id = 56; // int | Message ID

try {
    $result = $apiInstance->countAllMessages($atoken, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CountAllMessagesApi->countAllMessages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **id** | **int**| Message ID |

### Return type

[**\Swagger\Client\Model\Count**](../Model/Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

