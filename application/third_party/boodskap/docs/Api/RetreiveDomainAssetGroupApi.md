# Swagger\Client\RetreiveDomainAssetGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDomainAssetGroup**](RetreiveDomainAssetGroupApi.md#getDomainAssetGroup) | **GET** /domain/asset/group/get/{atoken}/{gid} | Retreive Domain Asset Group


# **getDomainAssetGroup**
> \Swagger\Client\Model\DomainAssetGroup getDomainAssetGroup($atoken, $gid)

Retreive Domain Asset Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveDomainAssetGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group id

try {
    $result = $apiInstance->getDomainAssetGroup($atoken, $gid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDomainAssetGroupApi->getDomainAssetGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group id |

### Return type

[**\Swagger\Client\Model\DomainAssetGroup**](../Model/DomainAssetGroup.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

