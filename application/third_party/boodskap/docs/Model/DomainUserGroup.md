# DomainUserGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**name** | **string** |  | 
**email** | **string** |  | [optional] 
**primary_phone** | **string** |  | [optional] 
**locale** | **string** |  | [optional] 
**timezone** | **string** |  | [optional] 
**work_start** | **int** |  | [optional] 
**work_end** | **int** |  | [optional] 
**work_days** | **int[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


