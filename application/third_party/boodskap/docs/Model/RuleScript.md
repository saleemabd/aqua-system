# RuleScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**script_args** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


