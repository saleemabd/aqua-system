<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><?php echo fb_text("settings"); ?></h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
						<li><a href="<?php echo site_url("/dashboard"); ?>"><?php echo fb_text("dashboard"); ?></a></li>
                        <li class="active"><?php echo fb_text("settings"); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
	   <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("settings_list"); ?></strong> </div>
            <div class="card-body">
			
			<?php if($this->session->flashdata('config_success')) {
				echo fb_message("success", $this->session->flashdata('config_success'));
			}?>
			
			
               <form action="<?php echo site_url("/common/save_settings"); ?>" method="post" class="form-horizontal">

			   <?php foreach($asettings as $skey => $sval): 
				$chk_str = ($sval)? ' checked="checked" ' : "";
			   ?>
 			      <div class="row form-group">
						<div class="col col-md-3">
							<label for="text-input" class=" form-control-label"><?php echo fb_text($skey); ?></label>
						 </div>
					 <div class="col-12 col-md-9">
						<?php echo form_checkbox($skey, 1, $sval, 'class="form-check-input"'); ?>
					 </div>
				  </div>
				<?php endforeach; ?> 
                <button type="submit" class="btn btn-primary"><?php echo fb_text("save"); ?></button>
                <button type="button" class="btn btn-secondary"><?php echo fb_text("clear"); ?></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  <!-- .content --> 
	
	
</div><!-- /#right-panel -->

<!-- Right Panel -->