<div class="card-header"> <strong class="card-title"><?php echo fb_text("edit_feed"); ?></strong> </div>
<div class="card-body">
  <form name="feed" id="feed-form" method="post" action="<?php echo base_url('feed/update');?>">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="feedname"><?php echo fb_text("feed_name"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="feedname" id="feedname" placeholder="Feed name" value="<?= $record['feedname']; ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="feed_type"><?php echo fb_text("feed_type"); ?><span class="error">*</span></label>
       <select name="feed_type" id="feed_type" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <option value="natural" <?php if($record['feed_type']=="natural"){ ?> selected="selected" <?php } ?>>Natural</option>
            <option value="chemical" <?php if($record['feed_type']=="chemical"){ ?> selected="selected" <?php } ?>>Chemical</option>
          </select>
      </div>
      <div class="form-group col-md-6">
        <label for="size"><?php echo fb_text("size"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="size" id="size" placeholder="<?php echo fb_text("size"); ?>" value="<?= $record['size']; ?>">
      </div>
     </div>
    
    <input type="hidden" name="rkey" value="<?=$rkey?>">
    <button type="submit" class="btn btn-primary"><?php echo fb_text("save"); ?></button>
    <button type="button" class="btn btn-secondary cancel"><?php echo fb_text("clear"); ?></button>
  </form>
</div>
<script type="application/javascript">

    $.validator.addMethod("chkduplicate", function(value, element, arg) {
		console.log(value);
		var pms = JSON.parse(arg);
		pms["search"] = value ;
		var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
		pms["rkey"] = rkey;
		var chk_url = site_url + "common/check_duplicate";
		var flg = false;
		$.ajax({
		    	type:"GET",
		    	data: pms,
		    	url: chk_url,
				async:false,
		}).done(function(resp){
			var rdata = JSON.parse(resp);
			console.log(resp);
			console.log(rdata.status);
			if(rdata.status == "success"){
				console.log("success");
				flg = true;
			} else{
				console.log("fail");
				flg = false;
			}
			
		}).fail(function(err){
			console.log(err);
		});
		console.log("123>>>>>>");
		//console.log(pms);
		return flg;
    }, "This field value already exists.");
	
	 $("#feed-form").validate({
		rules: {
			feedname: {
				required: true,
				chkduplicate: '{ "table_name": "feeds", "fld_name": "feedname"}',
			},
			feed_type:{
				required: true
			},
			size:{
				required: true,
				number: true,
				min:0
			}
		},
		messages: {
			
			feedname: {
					required: disp_text("err_fname"),
					chkduplicate: disp_text("err_fname_exists")
				},
				feed_type: {
					required: disp_text("err_ftype")
				},
				size: {
					required: disp_text("err_fsize"),
					number : disp_text("err_num"),
					min : disp_text("err_gt0")
				}
		}
	});
	$(document).on('click','.cancel', function(){
		$("#feed-form").find('input, select, textarea').val('');
	});
</script>	