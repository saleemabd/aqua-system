<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><?php echo fb_text("purge_all_caches"); ?></h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
						<li><a href="<?php echo site_url("/dashboard"); ?>"><?php echo fb_text("dashboard"); ?></a></li>
                        <li class="active"><?php echo fb_text("purge_all_caches"); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
	   <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("purge_all_caches"); ?></strong> </div>
            <div class="card-body">
			
			<?php if($this->session->flashdata('config_success')) {
				echo fb_message("success", $this->session->flashdata('config_success'));
			}?>
			
			
               <form action="<?php echo site_url("/common/purge_confirm_caches"); ?>" method="post" class="form-horizontal">
					<button type="submit" class="btn btn-primary"><?php echo fb_text("purge_all_caches"); ?></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  <!-- .content --> 
	
	
</div><!-- /#right-panel -->

<!-- Right Panel -->