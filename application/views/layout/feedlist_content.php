<div id="right-panel" class="right-panel">
  <?php $this->load->view('./include/top_menu'); ?>
  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1><?php echo fb_text("feed"); ?></h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="<?php echo site_url("/dashboard");?>"><?php echo fb_text("dashboard"); ?></a></li>
            <li class="active"><?php echo fb_text("feed_list"); ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("feed_list"); ?></strong> </div>
            <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>              
              <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <form name="search" id="srchfrm" action="<?php echo site_url("/feed/"); ?>" method="GET">
				<div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="dataTables_length" id="bootstrap-data-table_length">
                      <label> <?php echo fb_text("show"); ?>
                        <select name="no_items" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
                          <?php
							   $arr=array(10=>'10',20=>"20",50=>"50",-1=>"All");
							   foreach($arr as $key=>$val)
							   {							   
								   $selected='';
	   							   if(isset($_GET["no_items"]) and $_GET["no_items"]==$key)
								   $selected='selected="selected"';
								   echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
							   }							   
							    ?>
                        </select>
                        <?php echo fb_text("entries"); ?> </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div id="bootstrap-data-table_filter" class="dataTables_filter float-right">
                      <label><?php echo fb_text("search"); ?>:
                        <input type="search" name="search" class="form-control form-control-sm" placeholder="" value="<?php echo $search; ?>" />
                      </label>
                    </div>
                    <input type="hidden" name="sort_fld" value="<?php echo $sort_fld; ?>"/>
                    <input type="hidden" name="sort_dir" value="<?php echo $sort_dir; ?>"/>
                  </div>
                </div>
				</form>
                <table id="bootstrap-data-table" class="table table-striped table-bordered feedlist-table" role="grid">
                  <thead>
						   <tr role="row">
							  <?php echo $theader; ?>
						   </tr>
					</thead>
                  <tbody>
                    <?php foreach($result_set as $row): 
							  $source = $row["_source"];
							  $rkey = $row["_id"];
							  $CI =& get_instance();
							  $current_stock=$CI->getcurrentStock_feed($source["feedname"]);
							  
						   ?>
                    <tr role="row">
                      <td class=""><?php echo $source["feedname"]; ?></td>
                      <td><?php echo ucfirst($source["feed_type"]); ?></td>
                      <td><?php echo $source["size"]; ?></td>
                      <td><a href="#" data-id="" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <?php if($current_stock){?><a title="Edit Disabled" style=" cursor: none;" href="javascript:void(0);" data-id="<?=$rkey?>" ><i class="fa fa-pencil"></i></a><?php } else {?>
                              <a href="javascript:void(0);" data-id="<?=$rkey?>" class="edit-feed" ><i class="fa fa-pencil"></i></a><?php }?>&nbsp;&nbsp; <?php if($current_stock){?><a title="Delete Disabled" style=" cursor: none;" href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal"><i class="fa fa-trash"></i></a><?php } else {?><a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a><?php }?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="row">
                  <div class="col-sm-12 col-md-5"> 
                    <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>--> 
                  </div>
                  <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate"> <?php echo $page_links; ?> 
                      <!--<ul class="pagination">
						   <li class="paginate_button page-item previous" id="bootstrap-data-table_previous"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
						   <li class="paginate_button page-item active"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
						   <li class="paginate_button page-item"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
						   <li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
						   <li class="paginate_button page-item next" id="bootstrap-data-table_next"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="4" tabindex="0" class="page-link">Next</a></li>
						</ul> --> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card" id="feedform-card">
            <div class="card-header"> <strong class="card-title"><?php echo fb_text("add_feed"); ?></strong> </div>
            <div class="card-body">
              <form name="feed" id="feed-form" action="<?php echo base_url('feed/add_record');?>" method="post">
                <?php if($this->session->flashdata('success')) {
				?>
				<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
				<?php echo $this->session->flashdata('success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div> 
				<?php } ?>                 
				
				<?php if($this->session->flashdata('failed')) {
				?>
				<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
				<?php echo $this->session->flashdata('failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div> 
				<?php } ?>   
				 <?php if($this->session->flashdata('update_success')) {
				?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('update_failed')) {
				?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
                <?php } ?>    
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="feedname"><?php echo fb_text("feed_name"); ?><span class="error">*</span></label>
                    <input type="text" class="form-control" name="feedname" placeholder="<?php echo fb_text("feed_name"); ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="feed_type"><?php echo fb_text("feed_type"); ?><span class="error">*</span></label>
                    <select name="feed_type" id="feed_type" class="form-control">
                      <option selected value=""><?php echo fb_text("choose"); ?></option>
                      <option value="natural">Natural</option>
                      <option value="chemical">Chemical</option>
                    </select>
					</div>
                  <div class="form-group col-md-6">
                    <label for="size"><?php echo fb_text("size"); ?><span class="error">*</span></label>
                    <input type="text" class="form-control" name="size" placeholder="<?php echo fb_text("size"); ?>">
                  </div>
                </div>
                <button type="submit" class="btn btn-primary" ><?php echo fb_text("save"); ?></button>
                <button type="button" class="btn btn-secondary cancel"><?php echo fb_text("clear"); ?></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
</div>
<!-- /#right-panel --> 

<!-- Right Panel -->

<!-- Delete Modal --> 
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel"><?php echo fb_text("delete"); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               <?php echo fb_text("dmsg"); ?>
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('feed/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo fb_text("cancel"); ?></button>
            <button type="submit" class="btn btn-primary"><?php echo fb_text("confirm"); ?></button>
            </form>
        </div>
    </div>
</div>
</div>

<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="feednamemodel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("feed_type"); ?>:</label> &nbsp;&nbsp; <span id="feedtypemodel"></span></p>
            <p><label><?php echo fb_text("size"); ?>:</label>&nbsp;&nbsp; <span id="feedsizemodel"></span></p>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo fb_text("close"); ?></button>
        </div>
    </div>
</div>
</div>