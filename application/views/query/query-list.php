<?php if(!empty($search) && (!$chk_dflg) ): ?>
{
	"query": { "query_string":{ "query":"*__search__*"}},
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}
<?php elseif(!empty($search) && ($chk_dflg) ): ?>
{
	"query": {
		 "range" : {
			 "__range_fld__" : {
                "gte": __lowv__,
                "lte": __topv__,
				"boost" : 2.0
            }
		 }
    },
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}
<?php else: ?>
{
	"query": { "match_all":{} },
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}
<?php endif; ?>